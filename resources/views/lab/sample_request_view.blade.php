@extends('layout/contentLayoutMaster')
@section('title', 'Sample Request')


@section('vendor-style')
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/tables/datatable/dataTables.bootstrap5.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css')}}">
@endsection

@section('page-style')
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/plugins/forms/pickers/form-flat-pickr.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/pages/app-invoice.css')}}">

@endsection

@section('vendor-script')
<script src="{{asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('app-assets/vendors/js/tables/datatable/dataTables.bootstrap5.min.js')}}"></script>
<script src="{{asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('app-assets/vendors/js/tables/datatable/responsive.bootstrap4.js')}}"></script>
<script src="{{asset('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js')}}"></script>
<script src="{{asset('app-assets/vendors/js/forms/repeater/jquery.repeater.min.js')}}"></script>

@endsection

@section('page-script')
<script src="{{asset('app-assets/js/scripts/tables/table-datatables-basic.js')}}"></script>
<script src="{{asset('app-assets/js/scripts/pages/app-invoice.js')}}"></script>
<script src="{{asset('app-assets/js/scripts/components/components-navs.js')}}"></script>

<script>
assetPath = '../../../app-assets/';
  $(window).on('load', function() {
    $('.fl-datatables').dataTable({
      processing: true,
      dom: '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
    //   ajax: assetPath + 'data/ajax.php',
      language: {
        paginate: {
          // remove previous & next text from pagination
          previous: '&nbsp;',
          next: '&nbsp;'
        }
      }
    });
  });
</script>

@endsection

@section('app-content')

<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-8 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">{{$module_title}}</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html">Home</a>
                                </li>
                                <li class="breadcrumb-item active">{{$module_title}}
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-end col-md-4 col-12 d-md-block d-none">
                <div class="mb-1 breadcrumb-right">
                    <div class="dropdown" style="display:flex; float:right;">
                        <button class="btn btn-success btn-sm me-1" type="button">Add Lab Report</button>
                        <button class="btn btn-dark btn-sm " type="button">Edit Vendor Form</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">

            <section id="nav-filled">
                <div class="row match-height">
                    <!-- Justified Tabs starts -->
                    <div class="col-xl-12 col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <ul class="nav nav-tabs nav-justified" id="myTab2" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="profile-tab-justified" data-bs-toggle="tab" href="#profile-just" role="tab" aria-controls="profile-just" aria-selected="true">Sample Requirement</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="messages-tab-justified" data-bs-toggle="tab" href="#messages-just" role="tab" aria-controls="messages-just" aria-selected="false">Analysis Report</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="settings-tab-justified" data-bs-toggle="tab" href="#settings-just" role="tab" aria-controls="settings-just" aria-selected="false">Vendor Sample</a>
                                    </li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content pt-1">
                                    <div class="tab-pane active" id="profile-just" role="tabpanel" aria-labelledby="profile-tab-justified">
                                        <div class="accordion" id="accordionExample">
                                            <div class="accordion-item">
                                                <h2 class="accordion-header" id="headingOne">
                                                    <button class="accordion-button text-primary" type="button" data-bs-toggle="collapse" data-bs-target="#accordionOne" aria-expanded="true" aria-controls="accordionOne">
                                                        Request Id<span class="badge badge-light-primary">#35699</span>
                                                    </button>
                                                </h2>
                                                <div id="accordionOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                                    <div class="accordion-body">
                                                        <div class="row mb-1">
                                                            <div class="col-md-4">
                                                                <span class="fw-bold">Courier Name :</span>
                                                                <span>Delivery</span>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <span class="fw-bold">Tracking ID:</span>
                                                                <span>1811884866</span>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <span class="fw-bold">Courier Date :</span>
                                                                <span>28-July-2021</span>
                                                            </div>
                                                        </div>
                                                        <div class="table-responsive">
                                                            <table class="table">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Material</th>
                                                                        <th>Value</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td>Fly Ass</td>
                                                                        <td>3.2 gm</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Concreate</td>
                                                                        <td>2 gm</td>
                                                                    </tr>

                                                                </tbody>
                                                            </table>
                                                            <h6 class="fw-bold" style="float:right;">* Value Per 100 gm</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="tab-pane" id="messages-just" role="tabpanel" aria-labelledby="messages-tab-justified">
                                        <div class="accordion" id="accordionExample">
                                            <div class="accordion-item">
                                                <h2 class="accordion-header" id="headingOne">
                                                    <button class="accordion-button text-dark" type="button" data-bs-toggle="collapse" data-bs-target="#accordionLab1" aria-expanded="true" aria-controls="accordionOne">
                                                        Lab Analysis Report<span class="badge badge-light-success">#35699-L1</span>
                                                    </button>
                                                </h2>
                                                <div id="accordionLab1" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                                    <div class="accordion-body">
                                                        <div class="row mb-2">
                                                            <div class="col-md-6">
                                                                <span class="fw-bold">Report Time:</span>
                                                                <span>28-July-2021 12:35:52</span>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <span class="fw-bold">Report By:</span>
                                                                <span>Amit Goyel</span>
                                                            </div>
                                                        </div>
                                                
                                                        <div class="table-responsive">
                                                            <table class="table">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Material</th>
                                                                        <th>Value</th>
                                                                        <th>Visibility</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td>Fly Ass</td>
                                                                        <td>3.2 gm</td>
                                                                        <td>
                                                                            <div class="form-check form-check-success form-switch">
                                                                                <input type="checkbox" checked class="form-check-input" id="customSwitch4" />
                                                                            </div>
                                                                        </td>

                                                                    </tr>
                                                                    <tr>
                                                                        <td>Concreate</td>
                                                                        <td>2 gm</td>
                                                                        <td>
                                                                            <div class="form-check form-check-success form-switch">
                                                                                <input type="checkbox" checked class="form-check-input" id="customSwitch4" />
                                                                            </div>
                                                                        </td>
                                                                    </tr>

                                                                </tbody>
                                                            </table>
                                                            <h6 class="fw-bold" style="float:right;">* Value Per 100 gm</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="tab-pane" id="settings-just" role="tabpanel" aria-labelledby="settings-tab-justified">
                                        <div class="accordion" id="accordionExample">
                                            <div class="accordion-item">
                                                <h2 class="accordion-header" id="headingOne">
                                                    <button class="accordion-button text-dark" type="button" data-bs-toggle="collapse" data-bs-target="#accordionVendor1" aria-expanded="true" aria-controls="accordionOne">
                                                        Vendor Sample Report<span class="badge badge-light-info">#35699-L1-V1</span>
                                                    </button>
                                                </h2>
                                                <div id="accordionVendor1" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                                    <div class="accordion-body">
                                                        <div class="row mb-2">
                                                            <div class="col-md-6">
                                                                <span class="fw-bold">Vendor Name:</span>
                                                                <span>Gotam Kamant</span>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <span class="fw-bold">Uploaded At:</span>
                                                                <span> 28-July-2021 12:35:52</span>
                                                            </div>
                                                        </div>
                                                        <div class="table-responsive">
                                                            <table class="table">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Material</th>
                                                                        <th>Expected Value</th>
                                                                        <th>Current Value</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td>Fly Ass</td>
                                                                        <td>3.2 gm</td>
                                                                        <td>3.2 gm</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Concreate</td>
                                                                        <td>2 gm</td>
                                                                        <td>3.2 gm</td>
                                                                    </tr>

                                                                </tbody>
                                                            </table>
                                                            <h6 class="fw-bold" style="float:right;">* Value Per 100 gm</h6>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                            <div class="accordion-item">
                                                <h2 class="accordion-header" id="headingOne">
                                                    <button class="accordion-button text-dark" type="button" data-bs-toggle="collapse" data-bs-target="#accordionVendor2" aria-expanded="true" aria-controls="accordionOne">
                                                        Vendor Sample Report<span class="badge badge-light-info">#35699-L1-V2</span>
                                                    </button>
                                                </h2>
                                                <div id="accordionVendor2" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                                    <div class="accordion-body">
                                                        <div class="row mb-2">
                                                            <div class="col-md-6">
                                                                <span class="fw-bold">Vendor Name:</span>
                                                                <span>Gotam Kamant</span>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <span class="fw-bold">Uploaded At:</span>
                                                                <span> 28-July-2021 12:35:52</span>
                                                            </div>
                                                        </div>
                                                        <div class="table-responsive">
                                                            <table class="table">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Material</th>
                                                                        <th>Expected Value</th>
                                                                        <th>Current Value</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td>Fly Ass</td>
                                                                        <td>3.2 gm</td>
                                                                        <td>3.2 gm</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Concreate</td>
                                                                        <td>2 gm</td>
                                                                        <td>3.2 gm</td>
                                                                    </tr>

                                                                </tbody>
                                                            </table>
                                                            <h6 class="fw-bold" style="float:right;">* Value Per 100 gm</h6>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Justified Tabs ends -->
                </div>
            </section>
        </div>
    </div>
</div>

@endsection