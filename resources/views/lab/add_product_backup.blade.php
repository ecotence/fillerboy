@extends('layout/contentLayoutMaster')
@section('title', 'Add Product')

@section('page-style')
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
@endsection


@section('vendor-script')
<script src="{{asset('app-assets/vendors/js/forms/repeater/jquery.repeater.min.js')}}"></script>
@endsection


@section('page-script')
<script src="{{asset('app-assets/js/scripts/forms/form-tooltip-valid.js')}}"></script>
<script src="{{asset('app-assets/js/scripts/forms/form-repeater.js')}}"></script>

@endsection



@section('app-content')
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">{{$module_title}}</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/">Home</a>
                                </li>
                                <li class="breadcrumb-item active">{{$module_title}}
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
                <div class="mb-1 breadcrumb-right">
                    <div class="dropdown">
                        <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                        <div class="dropdown-menu dropdown-menu-end"><a class="dropdown-item" href="app-todo.html"><i class="me-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="me-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="me-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="me-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Basic Inputs start -->
            <section id="basic-input">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-xl-12 col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="basicInput">Product Name</label>
                                            <input type="text" class="form-control" id="basicInput" placeholder="Enter Product Name" />
                                        </div>
                                    </div>
                                    <div class="col-xl-12 col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="helpInputTop">Product Type <span class="text-danger">*</span></label>
                                            <select name="" id="" class="select2 form-select">
                                                <option value="">Cement</option>
                                                <option value="">Naptholin</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <button class="btn btn-success mt-1" type="submit">Add Product</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-xl-12 col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="basicInput">Product Size</label>
                                            <input type="text" class="form-control" id="basicInput" placeholder="Enter Product Size" />
                                        </div>
                                    </div>
                                    <div class="col-xl-12 col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="disabledInput">Product Color</label>
                                            <select name="" id="" class="select2 form-select">
                                                <option value="">Red</option>
                                                <option value="">Blue</option>
                                                <option value="">Pink</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Basic Inputs end -->



        </div>
    </div>
</div>
@endsection