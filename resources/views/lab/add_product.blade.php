@extends('layout/contentLayoutMaster')
@section('title', 'Add Product')

@section('page-style')
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
@endsection


@section('vendor-script')
<script src="{{asset('app-assets/vendors/js/forms/repeater/jquery.repeater.min.js')}}"></script>
@endsection


@section('page-script')
<script src="{{asset('app-assets/js/scripts/forms/form-repeater.js')}}"></script>
{{-- <script src="{{asset('app-assets/js/scripts/forms/form-tooltip-valid.js')}}"></script> --}}

@endsection



@section('app-content')
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">{{$module_title}}</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/">Home</a>
                                </li>
                                <li class="breadcrumb-item active">{{$module_title}}
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
                <div class="mb-1 breadcrumb-right">
                    <button class="btn btn-dark" type="button" data-bs-toggle="modal" data-bs-target="#exampleModalCenter"><i data-feather='file-text' class="me-25"></i>Preliminary Report Setup</button>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Basic Inputs start -->
            <section id="basic-input">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-xl-6 col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="basicInput">Product Name</label>
                                            <input type="text" class="form-control" id="basicInput" placeholder="Enter Product Name" />
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="helpInputTop">Sample <span class="text-danger">*</span></label>
                                            <select name="" id="" class="select2 form-select">
                                                <option value="">Select Sample</option>
                                                <option value="">Sample 002</option>
                                                <option value="">Sample 003</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <form action="#" class="repeater-default">
                                    <div class="row " data-repeater-list="product-details">
                                        <div class="row mt-1" data-repeater-item>
                                            <div class="col-md-4">
                                            <div class="form-floating">    
                                                    <input type="text" class="form-control " id="name" placeholder="" />
                                                    <label class="form-label" for="name">Name</label>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-floating">    
                                                    <input type="text" class="form-control " id="value" placeholder="" />
                                                    <label class="form-label" for="value">Value</label>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-check form-check-success form-switch mt-25">
                                                    <input type="checkbox" checked class="form-check-input" id="customSwitch4" />
                                                    <label class="form-check-label" for="Product">Visibility</label>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-12 mb-50 fl-right">
                                                <div class="mb-1">
                                                    <button class="btn btn-outline-danger text-nowrap px-1" data-repeater-delete type="button">
                                                        <i data-feather="x" class="me-25"></i>
                                                        <span>Delete</span>
                                                    </button>
                                                </div>
                                            </div>
                                            <hr>
                                        </div>
                                    </div>
                                <button class="btn btn-success mt-1 fl-right" type="button">Add Product</button>
                                <button class="btn btn-outline-primary mt-1 fl-right me-1" type="button" data-repeater-create> <i data-feather="plus" class="me-25"></i>Add Details</button>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Basic Inputs end -->

        </div>
    </div>
</div>


<div class="modal fade" id="exampleModalCenter" tabindex="-1" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Preliminary Report Form</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="#" class="repeater-default">
                    <div class="row " data-repeater-list="product-details">
                        <div class="row mt-1" data-repeater-item>
                                <div class="col-md-10">
                                    <div class="form-floating">    
                                        <input type="text" class="form-control " id="name" placeholder="" />
                                        <label class="form-label" for="name">Field Name</label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="mb-1 fl-right">
                                        <button class="btn btn-outline-danger btn-icon" data-repeater-delete type="button">
                                            <i data-feather='trash-2'></i>
                                        </button>
                                    </div>
                                </div>
                            <hr>
                        </div>
                    </div>
                <button class="btn btn-outline-dark" type="button" data-repeater-create> <i data-feather="plus" class="me-25"></i>Add Field</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-bs-dismiss="modal">Save</button>
            </div>
        </div>
    </div>
</div>

@endsection