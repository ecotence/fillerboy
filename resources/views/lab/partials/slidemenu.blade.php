<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item me-auto"><a class="navbar-brand" href="/"><span class="brand-logo"><img src="{{asset('app-assets/images/logo/logo.png')}}" alt=""></span>
                    <h2 class="brand-text text-dark">Filler Boy</h2>
                </a></li>
            <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pe-0" data-bs-toggle="collapse"><i class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i><i class="d-none d-xl-block collapse-toggle-icon font-medium-4  text-primary" data-feather="disc" data-ticon="disc"></i></a></li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">

            <li class="nav-item @if($module == 'dashboard') active @endif"><a class="d-flex align-items-center" href="/dashboard"><i data-feather="home"></i><span class="menu-title text-truncate" data-i18n="Dashboard">Dashboard</span></a></li>
            <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="package"></i><span class="menu-title text-truncate" data-i18n="Products">Products</span></a>
                <ul class="menu-content">
                    <li class="@if($module == 'add_product_type') active @endif"><a class="d-flex align-items-center" href="/add-product-type"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Add Product Type">Add Product Type</span></a></li>
                    <li class="@if($module == 'manage_product_type') active @endif"><a class="d-flex align-items-center" href="/manage-product-type"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Manage Product Type">Manage Type</span></a></li>
                    <li class="@if($module == 'add_product') active @endif"><a class="d-flex align-items-center" href="/add-product"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Add Product">Add Product</span></a></li>
                    <li class="@if($module == 'manage_products') active @endif"><a class="d-flex align-items-center" href="/manage-products"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Manage Products">Manage Products</span></a></li>
                </ul>
            </li>
            <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="shopping-bag"></i><span class="menu-title text-truncate" data-i18n="Orders">Orders</span></a>
                <ul class="menu-content">
                    <li class="@if($module == 'manage_orders') active @endif"><a class="d-flex align-items-center" href="/manage-orders"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Manage Orders">Manage Orders</span></a></li>
                </ul>
            </li>

            <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather='archive'></i><span class="menu-title text-truncate" data-i18n="Samples">Samples</span></a>
                <ul class="menu-content">
                    <li class="@if($module == 'sample_request') active @endif"><a class="d-flex align-items-center" href="/sample-request"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Sample Request">Sample Request</span></a></li>
                    <li class="@if($module == 'vendor_sample') active @endif"><a class="d-flex align-items-center" href="/vendor-sample"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Vendor Sample">Vendor Sample</span></a></li>
                    <li class="@if($module == 'add_sample') active @endif"><a class="d-flex align-items-center" href="/add-sample"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Add Sample">Add Sample</span></a></li>
                    <li class="@if($module == 'view_matched_samples') active @endif"><a class="d-flex align-items-center" href="/view-matched-samples"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Matched Samples">Matched Samples</span></a></li>
                    <li class="@if($module == 'manage_samples') active @endif"><a class="d-flex align-items-center" href="/manage-samples"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Manage Samples">Manage Samples</span></a></li>
                    <li class="@if($module == 'pending_samples') active @endif"><a class="d-flex align-items-center" href="/pending-samples"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Pending Samples">Pending Samples</span></a></li>
                </ul>
            </li>


            <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="video"></i><span class="menu-title text-truncate" data-i18n="Meetings">Meetings</span></a>
                <ul class="menu-content">
                    <li class="@if($module == 'create_meeting') active @endif"><a class="d-flex align-items-center" href="/create-meeting"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Create Meeting">Create Meeting</span></a></li>
                    <li class="@if($module == 'manage_meetings') active @endif"><a class="d-flex align-items-center" href="/manage-meetings"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Manage Meetings">Manage Meetings</span></a></li>
                </ul>
            </li>

            <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="folder"></i><span class="menu-title text-truncate" data-i18n="Documents">Documents</span></a>
                <ul class="menu-content">
                    <li class="@if($module == 'add_documents') active @endif"><a class="d-flex align-items-center" href="/add-documents"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Add Research Paper">Add Research Paper</span></a></li>
                    <li class="@if($module == 'manage_documents') active @endif"><a class="d-flex align-items-center" href="/manage-documents"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n=">Manage Documents">Manage Documents</span></a></li>
                </ul>
            </li>

            <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="life-buoy"></i><span class="menu-title text-truncate" data-i18n="Tickets">Tickets</span></a>
                <ul class="menu-content">
                    <li class="@if($module == 'create_ticket') active @endif"><a class="d-flex align-items-center" href="/create-ticket"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Attendance">Create Ticket</span></a></li>
                    <li class="@if($module == 'open_ticket') active @endif"><a class="d-flex align-items-center" href="/open-ticket"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Open Tickets">Open Tickets</span></a></li>
                    <li class="@if($module == 'tickets') active @endif"><a class="d-flex align-items-center" href="/tickets"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="All Tickets">All Tickets</span></a></li>
                </ul>
            </li>
            <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather='dollar-sign'></i><span class="menu-title text-truncate" data-i18n="Payroll">Payroll</span></a>
                <ul class="menu-content">
                    <li class="@if($module == 'attendance') active @endif"><a class="d-flex align-items-center" href="/attendance"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Attendance">Attendance</span></a></li>
                    <li class="@if($module == 'leaves') active @endif"><a class="d-flex align-items-center" href="/leaves"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Leaves">Leaves</span></a></li>
                    <li class="@if($module == 'payslips') active @endif"><a class="d-flex align-items-center" href="/payslips"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Pay Slips">Pay Slips</span></a></li>
                </ul>
            </li>

            <li class=" nav-item @if($module == 'chat') active @endif"><a class="d-flex align-items-center" href="/chat"><i data-feather="message-square"></i><span class="menu-title text-truncate" data-i18n="Chat">Chat</span></a></li>




        </ul>
    </div>
</div>