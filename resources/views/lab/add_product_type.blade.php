@extends('layout/contentLayoutMaster')
@section('title', 'Add Product Type')

@section('page-style')
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
    <style>
        .mr-1{
            margin-right:5px !important;
        }
        .add-product-table tbody tr td{
            padding: 0.72rem 1rem;
        }
    </style>
@endsection


@section('page-script')
<script src="{{asset('app-assets/js/scripts/forms/form-tooltip-valid.js')}}"></script>
@endsection



@section('app-content')
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">{{$module_title}}</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/">Home</a>
                                </li>
                                <li class="breadcrumb-item active">{{$module_title}}
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
                <div class="mb-1 breadcrumb-right">
                    <div class="dropdown">
                        <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                        <div class="dropdown-menu dropdown-menu-end"><a class="dropdown-item" href="app-todo.html"><i class="me-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="me-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="me-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="me-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Basic Inputs start -->
            <section id="basic-input">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-xl-12 col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="basicInput">Product Type</label>
                                            <input type="text" class="form-control" id="basicInput" placeholder="Enter Product Type" />
                                        </div>
                                    </div>
                                </div>
                                <button class="btn btn-success mt-1" style="float:right" type="submit">Add Product Type</button>
                                <button type="button" class="btn btn-outline-secondary mt-1">Add Fields</button>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Form Preview</h4>
                            </div>
                            <div class="card-body">
                                
                                <div class="row">
                                    <table class="table add-product-table">
                                        <tbody>
                                            <tr>
                                                <td style="vertical-align: middle;">Text Field</td>
                                                <td><input type="text" class="form-control" placeholder="Enter Label"></td>
                                                <td style="vertical-align: middle; display:flex;">
                                                    <button type="button" class="btn btn-icon btn-warning waves-effect waves-float waves-light mr-1"><i data-feather='edit'></i></button>
                                                    <button type="button" class="btn btn-icon btn-danger waves-effect waves-float waves-light"><i data-feather='trash'></i></button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: middle;">Dropdown</td>
                                                <td><input type="text" class="form-control" placeholder="Enter Label"></td>
                                                <td style="vertical-align: middle; display:flex;">
                                                    <button type="button" class="btn btn-icon btn-warning waves-effect waves-float waves-light mr-1"><i data-feather='edit'></i></button>
                                                    <button type="button" class="btn btn-icon btn-danger waves-effect waves-float waves-light"><i data-feather='trash'></i></button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Basic Inputs end -->



        </div>
    </div>
</div>
@endsection