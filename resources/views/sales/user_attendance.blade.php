@extends('layout/contentLayoutMaster')
@section('title', 'User Attendance')


@section('vendor-style')
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/tables/datatable/dataTables.bootstrap5.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css')}}">
@endsection

@section('page-style')
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/plugins/forms/pickers/form-flat-pickr.css')}}">
@endsection

@section('vendor-script')
<script src="{{asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('app-assets/vendors/js/tables/datatable/dataTables.bootstrap5.min.js')}}"></script>
<script src="{{asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('app-assets/vendors/js/tables/datatable/responsive.bootstrap4.js')}}"></script>
<script src="{{asset('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js')}}"></script>
@endsection

@section('page-script')
<script src="{{asset('app-assets/js/scripts/tables/table-datatables-basic.js')}}"></script>
<script>
assetPath = '../../../app-assets/';
  $(window).on('load', function() {
    $('.fl-datatables').dataTable({
      processing: true,
      dom: '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
    //   ajax: assetPath + 'data/ajax.php',
      language: {
        paginate: {
          // remove previous & next text from pagination
          previous: '&nbsp;',
          next: '&nbsp;'
        }
      }
    });
  });
</script>

@endsection


@section('app-content')

<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">{{$module_title}}</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item"><a href="#">{{$module_title}}</a></li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
           
        </div>
        <div class="content-body">
            <!-- Basic table -->
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <table class="fl-datatables table">
                                <thead>
                                    <tr>
                                        <th>User</th>
                                        <th>Date</th>
                                        <th>Status</th>
                                        <th>Clock In</th>
                                        <th>Clock Out</th>
                                        <th>Late</th>
                                        <th>Early Leaving</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Anil Chopra</td>
                                        <td>24-07-2021</td>
                                        <td><span class="badge bg-success">Present</span></td>
                                        <td>09:13:56 AM</td>
                                        <td>07:10:00 PM</td>
                                        <td>00:12:36</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td>Anil Chopra</td>
                                        <td>23-07-2021</td>
                                        <td><span class="badge bg-warning">Leave</span></td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td>Anil Chopra</td>
                                        <td>22-07-2021</td>
                                        <td><span class="badge bg-danger">Absent</span></td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td>Vineeth Kumar</td>
                                        <td>22-07-2021</td>
                                        <td><span class="badge bg-success">Present</span></td>
                                        <td>09:13:56 AM</td>
                                        <td>07:10:00 PM</td>
                                        <td>00:12:36</td>
                                        <td>-</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Modal to add new record -->

            </section>
            <!--/ Basic table -->


        </div>
    </div>
</div>

@endsection