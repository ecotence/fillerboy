@extends('layout/contentLayoutMaster')
@section('title', 'Create Order')

@section('page-style')
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
@endsection


@section('page-script')
<script src="{{asset('app-assets/js/scripts/forms/form-tooltip-valid.js')}}"></script>

<script>
$(function() {
    $('#customized-form').hide(); 
    $('#order_product').change(function(){

    if($('#product_type').val() == 'customize') {

        if($('#order_product').val() == 'cement') {
            $('#customized-form').show(); 
        } else {
            $('#customized-form').hide(); 
        } 
    }

    });
});
</script>

@endsection



@section('app-content')
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">{{$module_title}}</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/">Home</a>
                                </li>
                                <li class="breadcrumb-item active">{{$module_title}}
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
                <div class="mb-1 breadcrumb-right">
                    <div class="dropdown">
                        <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                        <div class="dropdown-menu dropdown-menu-end"><a class="dropdown-item" href="app-todo.html"><i class="me-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="me-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="me-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="me-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Basic Inputs start -->
            <section id="basic-input">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-xl-6 col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="helpInputTop">Customer <span class="text-danger">*</span></label>
                                            <select name="" id="" class="select2 form-select">
                                                <option value="">Amit Parser</option>
                                                <option value="">Rakesh Goutam</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="helpInputTop">Select Product Type<span class="text-danger">*</span></label>
                                            <select name="" id="product_type" class="select2 form-select">
                                                <option value="">Select Product Type</option>
                                                <option value="predefine">Predefined Product</option>
                                                <option value="customize">Customize Product</option>
                                            </select>
                                        </div>
                                    </div>
                                   
                                    <div class="col-xl-12 col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="helpInputTop">Select Product<span class="text-danger">*</span></label>
                                            <select name="" id="order_product" class="select2 form-select">
                                                <option>Select Product</option>
                                                <option value="cement">Cement</option>
                                                <option value="naptha">Naptholin</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                           
                            
                                <div class="row" id="customized-form">
                                    <div class="col-xl-6 col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="helpInputTop">Carbon</label>
                                            <input type="text" name="" id="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="helpInputTop">Nitrogen</label>
                                            <input type="text" name="" id="" class="form-control">
                                        </div>
                                    </div>
                                </div>        
                            

                                <div class="row">
                                    <div class="col-xl-6 col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="helpInputTop">Delivery Address <span class="text-danger">*</span></label>
                                            <select name="" id="" class="select2 form-select">
                                                <option value="">Godown One</option>
                                                <option value="">Godown Two</option>
                                            </select>
                                        </div>
                                    </div>



                                    <div class="col-xl-6 col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="helpInputTop">Remarks</label>
                                            <input type="text" name="" id="" class="form-control">
                                        </div>
                                    </div>

                                </div>
                                <button class="btn btn-success mt-1" type="submit">Place Order</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Basic Inputs end -->



        </div>
    </div>
</div>
@endsection