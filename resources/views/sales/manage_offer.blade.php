@extends('layout/contentLayoutMaster')
@section('title', 'View Products')


@section('vendor-style')
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/tables/datatable/dataTables.bootstrap5.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css')}}">
@endsection

@section('page-style')
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/plugins/forms/pickers/form-flat-pickr.css')}}">
@endsection

@section('vendor-script')
<script src="{{asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('app-assets/vendors/js/tables/datatable/dataTables.bootstrap5.min.js')}}"></script>
<script src="{{asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('app-assets/vendors/js/tables/datatable/responsive.bootstrap4.js')}}"></script>
<script src="{{asset('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js')}}"></script>
@endsection

@section('page-script')
<script src="{{asset('app-assets/js/scripts/tables/table-datatables-basic.js')}}"></script>
<script>
assetPath = '../../../app-assets/';
  $(window).on('load', function() {
    $('.fl-datatables').dataTable({
      processing: true,
      dom: '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
    //   ajax: assetPath + 'data/ajax.php',
      language: {
        paginate: {
          // remove previous & next text from pagination
          previous: '&nbsp;',
          next: '&nbsp;'
        }
      }
    });
  });

$("#offer_type").select2({
    placeholder: "Select Offer Type",
    allowClear: true
});



$('#customer_select').hide();

$('input[type=radio][name=offer_for]').change(function() {
    if (this.value == 'all') {
        $('#customer_select').hide();
    }
    else if (this.value == 'selected') {
        
        $('#customer_select').show();
        $("#offer_customer").select2({
            placeholder: "Select Customers",
            allowClear: true
        });
    }
});

$('#offer_type').change(function(){
    if (this.value == 'flat') {
        $('.offer-disable').prop('disabled', false);
        $('.offer-disable-percent').prop('disabled', true);
        // $('#customer_name').html(this.value);
    }else if(this.value == 'percent'){
        $('.offer-disable').prop('disabled', false);
        $('.offer-disable-percent').prop('disabled', false);
    }
});
    






</script>

@endsection


@section('app-content')

<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">{{$module_title}}</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">Home</li>
                                <li class="breadcrumb-item"><a href="#">{{$module_title}}</a></li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
                <div class="mb-1 breadcrumb-right">
                    <div class="dropdown">
                        <button class="btn btn-primary btn-round" type="button" data-bs-toggle="modal" data-bs-target="#offerModal"> Add Offer</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Basic table -->
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <table class="fl-datatables table">
                                <thead>
                                    <tr>
                                        <th>Offer for</th>
                                        <th>Offer Type</th>
                                        <th>Min. Order Value</th>
                                        <th>Discount</th>
                                        <th>Max Discount</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>All Customer</td>
                                        <td>Flat</td>
                                        <td>25000</td>
                                        <td>2500</td>
                                        <td>-</td>
                                        <td>
                                            <a href="javascript:;"><i data-feather="edit"></i></a>
                                            <a href="javascript:;"><i data-feather="trash-2"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Selected</td>
                                        <td>Percent</td>
                                        <td>25000</td>
                                        <td>25</td>
                                        <td>2000</td>
                                        <td>
                                            <a href="javascript:;"><i data-feather="edit"></i></a>
                                            <a href="javascript:;"><i data-feather="trash-2"></i></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- Modal to add new record -->

            </section>
            <!--/ Basic table -->


        </div>
    </div>
</div>

<div class="modal fade" id="offerModal" tabindex="-1" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Add Offer</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xl-12 col-md-6 col-12">
                        <div class="mb-1">
                            <label class="form-label" for="helpInputTop">Offer Type</label>
                            <select name="" id="offer_type" class="select2 form-select">
                                <option value="flat">Flat</option>
                                <option value="percent">Percentage</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xl-12 col-md-6 col-12">
                        <div class="mb-1">
                            <label class="form-label" for="basicInput">Discount <span class="text-danger">*</span></label>
                            <input type="text" class="form-control offer-disable" disabled placeholder="Enter Discount" />
                        </div>
                    </div>
                    <div class="col-xl-12 col-md-6 col-12">
                        <div class="mb-1">
                            <label class="form-label" for="basicInput">Min. Order Value <span class="text-danger">*</span></label>
                            <input type="text" class="form-control offer-disable" disabled placeholder="Enter Min. Order Value" />
                        </div>
                    </div>
                    <div class="col-xl-12 col-md-6 col-12">
                        <div class="mb-1">
                            <label class="form-label" for="basicInput">Max Discount </label>
                            <input type="text" class="form-control offer-disable-percent" disabled placeholder="Enter Max Discount" />
                        </div>
                    </div>

                    <div class="col-xl-12 col-md-6 col-12">
                        <div class="mt-75">
                            <span class="me-1">Offer for </span>
                            <div class="form-check form-check-inline form-check-primary">
                                <input class="form-check-input" type="radio" name="offer_for" id="inlineRadio1" value="all" />
                                <label class="form-check-label" for="inlineRadio1">All Customers</label>
                            </div>
                            <div class="form-check form-check-inline form-check-primary">
                                <input class="form-check-input" type="radio" name="offer_for" id="inlineRadio2" value="selected" />
                                <label class="form-check-label" for="inlineRadio2">Selected Customer</label>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-12 col-md-6 col-12 mt-2" id="customer_select">
                        <div class="mb-1">
                            <select name="" id="offer_customer" class="customer-select form-select" multiple>
                                <option value="a">Anil Dubey</option>
                                <option value="b">Nitin Gowel</option>
                            </select>
                        </div>
                    </div>

                    

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-bs-dismiss="modal">Add Offer</button>
            </div>
        </div>
    </div>
</div>


@endsection