@extends('layout/contentLayoutMaster')
@section('title', 'Manage Samples')


@section('vendor-style')
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/tables/datatable/dataTables.bootstrap5.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/tables/datatable/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css')}}">
@endsection

@section('page-style')
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/plugins/forms/pickers/form-flat-pickr.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/pages/app-invoice.css')}}">

@endsection

@section('vendor-script')
<script src="{{asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('app-assets/vendors/js/tables/datatable/dataTables.bootstrap5.min.js')}}"></script>
<script src="{{asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('app-assets/vendors/js/tables/datatable/responsive.bootstrap4.js')}}"></script>
<script src="{{asset('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js')}}"></script>
<script src="{{asset('app-assets/vendors/js/forms/repeater/jquery.repeater.min.js')}}"></script>

@endsection

@section('page-script')
<script src="{{asset('app-assets/js/scripts/tables/table-datatables-basic.js')}}"></script>
<script src="{{asset('app-assets/js/scripts/pages/app-invoice.js')}}"></script>
<script src="{{asset('app-assets/js/scripts/components/components-navs.js')}}"></script>

<script>
assetPath = '../../../app-assets/';
  $(window).on('load', function() {
    $('.fl-datatables').dataTable({
      processing: true,
      dom: '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
    //   ajax: assetPath + 'data/ajax.php',
      language: {
        paginate: {
          // remove previous & next text from pagination
          previous: '&nbsp;',
          next: '&nbsp;'
        }
      }
    });
  });
</script>

@endsection

@section('app-content')

<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-8 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">{{$module_title}}</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html">Home</a>
                                </li>
                                <li class="breadcrumb-item active">{{$module_title}}
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="content-body">

            <section id="nav-filled">
                <div class="row match-height">
                    <!-- Justified Tabs starts -->
                    <div class="col-xl-12 col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <ul class="nav nav-tabs nav-justified" id="myTab2" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="profile-tab-justified" data-bs-toggle="tab" href="#profile-just" role="tab" aria-controls="profile-just" aria-selected="true">Inhouse Sample</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="messages-tab-justified" data-bs-toggle="tab" href="#messages-just" role="tab" aria-controls="messages-just" aria-selected="false">Customers Sample</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="settings-tab-justified" data-bs-toggle="tab" href="#settings-just" role="tab" aria-controls="settings-just" aria-selected="false">Vendors Sample</a>
                                    </li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content pt-1">
                                    <div class="tab-pane active" id="profile-just" role="tabpanel" aria-labelledby="profile-tab-justified">
                                        <table class="fl-datatables table">
                                            <thead>
                                                <tr>
                                                    <th>Sample Id</th>
                                                    <th>Sample Name</th>
                                                    <th>Create Date</th>
                                                    <th class="text-center">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><span class="badge bg-success">#FL3123143</span></td>
                                                    <td>Cement-1</td>
                                                    <td>26-Jul-2021</td>
                                                    <td class="text-center">
                                                        <a href="javascript:;"><i data-feather='shopping-bag'></i></i></a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="tab-pane" id="messages-just" role="tabpanel" aria-labelledby="messages-tab-justified">
                                        <table class="fl-datatables table">
                                            <thead>
                                                <tr>
                                                    <th>Sample Id</th>
                                                    <th>Customer Name</th>
                                                    <th>Create Date</th>
                                                    <th class="text-center">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><span class="badge bg-primary">#FL3123121</span></td>
                                                    <td>Gourab Kamant</td>
                                                    <td>26-Jul-2021</td>
                                                    <td class="text-center">
                                                        <a href="javascript:;"><i data-feather='shopping-bag'></i></i></a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="tab-pane" id="settings-just" role="tabpanel" aria-labelledby="settings-tab-justified">
                                        <table class="fl-datatables table">
                                            <thead>
                                                <tr>
                                                    <th>Sample Id</th>
                                                    <th>Vendor Name</th>
                                                    <th>Create Date</th>
                                                    <th class="text-center">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><span class="badge bg-primary">#FL3123121</span></td>
                                                    <td>Sunil Gupta</td>
                                                    <td>26-Jul-2021</td>
                                                    <td class="text-center">
                                                        <a href="javascript:;"><i data-feather='shopping-bag'></i></i></a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Justified Tabs ends -->
                </div>
            </section>
        </div>
    </div>
</div>

@endsection