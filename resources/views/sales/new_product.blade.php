@extends('layout/contentLayoutMaster')
@section('title', 'New Product')

@section('page-style')
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
@endsection


@section('vendor-script')
<script src="{{asset('app-assets/vendors/js/forms/repeater/jquery.repeater.min.js')}}"></script>
@endsection


@section('page-script')
<script src="{{asset('app-assets/js/scripts/forms/form-repeater.js')}}"></script>
<script>

$(document).ready(function() { 

    $('#quotation_list').hide();

$('input[type=radio][name=visibility]').change(function() {
    if (this.value == 'all') {
        $('#priceForselected').hide();
        $('#priceForall').show();
        $('#quotation_list').hide();
        
    }
    else if (this.value == 'selected') {
        $('#priceForall').hide();
        $('#priceForselected').show();
        $('#quotation_list').show();
        
    }
});


$('#customer_list').change(function(){
    if (this.value != '') {

        // ('#priceModal').modal('show');
        // alert(this.value);
        $('#priceModal').modal('show');
        $('#customer_name').html(this.value);
    }
})
    

});
    
</script>
@endsection



@section('app-content')
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">{{$module_title}}</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/">Home</a>
                                </li>
                                <li class="breadcrumb-item active">{{$module_title}}
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
                <div class="mb-1 breadcrumb-right">
                    <div class="dropdown">
                        <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                        <div class="dropdown-menu dropdown-menu-end"><a class="dropdown-item" href="app-todo.html"><i class="me-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="me-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="me-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="me-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <!-- Basic Inputs start -->
            <section id="basic-input">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-xl-6 col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="basicInput">Product Name</label>
                                            <input type="text" class="form-control" readonly value="Cement" />
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="basicInput">Product Display Name <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" placeholder="Enter Display Name" />
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="basicInput">Stock <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" placeholder="Enter Stock" />
                                        </div>
                                    </div>
                                   
                                    <div class="col-xl-6 col-md-6 col-12 mt-2">
                                        <div class="mt-75">
                                            <span class="me-1">Visible to </span>
                                            <div class="form-check form-check-inline form-check-success">
                                                <input class="form-check-input" type="radio" name="visibility" id="inlineRadio1" value="all" />
                                                <label class="form-check-label" for="inlineRadio1">All Customers</label>
                                            </div>
                                            <div class="form-check form-check-inline form-check-danger">
                                                <input class="form-check-input" type="radio" name="visibility" id="inlineRadio2" value="selected" />
                                                <label class="form-check-label" for="inlineRadio2">Selected Customer</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="priceForall" style="display: none;">
                                    <div class="col-xl-6 col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="basicInput">Price <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" placeholder="Enter Price" />
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="basicInput">Unit <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" placeholder="Enter Unit" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="priceForselected" style="display: none;">
                                    <div class="col-xl-12 col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="helpInputTop">Customers</label>
                                            <select name="" id="customer_list" class="select2 form-select">
                                                <option value="">Select Customer</option>
                                                <option value="Anil Dubey">Anil Dubey</option>
                                                <option value="Nitin Chowbey">Nitin Chowbey</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>


                                <button class="btn btn-success mt-1 fl-right" type="submit">Publish Product</button>
                            </div>
                        </div>
                    </div>

                    <div class="row" id="quotation_list" style="display:none;">

                        <div class="col-md-4 col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Anil Dubey</h4>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li>
                                                <a data-action="collapse"><i data-feather="chevron-down"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <div class="row ">
                                            <div class="col-md-6 mb-50">
                                                <span class="fw-bold">Price:</span>
                                                <span>62.00</span>
                                            </div>
                                            <div class="col-md-6">
                                                <span class="fw-bold">Unit:</span>
                                                <span>Ton</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Nitin Chowbey</h4>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li>
                                                <a data-action="collapse"><i data-feather="chevron-down"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <div class="row ">
                                            <div class="col-md-6 mb-50">
                                                <span class="fw-bold">Price:</span>
                                                <span>62.00</span>
                                            </div>
                                            <div class="col-md-6">
                                                <span class="fw-bold">Unit:</span>
                                                <span>Ton</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </section>
            <!-- Basic Inputs end -->

        </div>
    </div>
</div>


<div class="modal fade" id="priceModal" tabindex="-1" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Price For - <span id="customer_name"></span></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xl-6 col-md-6 col-12">
                        <div class="mb-1">
                            <label class="form-label" for="basicInput">Price <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" placeholder="Enter Price" />
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6 col-12">
                        <div class="mb-1">
                            <label class="form-label" for="basicInput">Unit <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" placeholder="Enter Unit" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-bs-dismiss="modal">Save</button>
            </div>
        </div>
    </div>
</div>



@endsection