<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item me-auto"><a class="navbar-brand" href="/"><span class="brand-logo"><img src="{{asset('app-assets/images/logo/logo.png')}}" alt=""></span>
                    <h2 class="brand-text text-dark">Filler Boy</h2>
                </a></li>
            <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pe-0" data-bs-toggle="collapse"><i class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i><i class="d-none d-xl-block collapse-toggle-icon font-medium-4  text-primary" data-feather="disc" data-ticon="disc"></i></a></li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">

            <li class="nav-item @if($module == 'dashboard') active @endif"><a class="d-flex align-items-center" href="/dashboard"><i data-feather="home"></i><span class="menu-title text-truncate" data-i18n="Dashboard">Dashboard</span></a></li>
            <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="package"></i><span class="menu-title text-truncate" data-i18n="Products">Products</span></a>
                <ul class="menu-content">
                    <li class="@if($module == 'view_product') active @endif"><a class="d-flex align-items-center" href="/view-product"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="View Products">View Products</span></a></li>
                </ul>
            </li>
            <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="shopping-bag"></i><span class="menu-title text-truncate" data-i18n="Products">Orders</span></a>
                <ul class="menu-content">
                    <li class="@if($module == 'create_order') active @endif"><a class="d-flex align-items-center" href="/create-order"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Create Order">Create Order</span></a></li>
                    <li class="@if($module == 'manage_orders') active @endif"><a class="d-flex align-items-center" href="/manage-orders"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Manage Orders">Manage Orders</span></a></li>
                    <li class="@if($module == 'manage_offer') active @endif"><a class="d-flex align-items-center" href="/manage-offer"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Offer Management">Offer Management</span></a></li>
                </ul>
            </li>
            <li class=" nav-item @if($module == 'stock_report') active @endif"><a class="d-flex align-items-center" href="/stock-report"><i data-feather="server"></i><span class="menu-title text-truncate" data-i18n="Stock">Stock</span></a></li>

            <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="users"></i><span class="menu-title text-truncate" data-i18n="User Management">User Management</span></a>
                <ul class="menu-content">
                    <li class="@if($module == 'add_user') active @endif"><a class="d-flex align-items-center" href="/add-user"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Add User">Add User</span></a></li>
                    <li class="@if($module == 'manage_users') active @endif"><a class="d-flex align-items-center" href="/manage-users"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Manage User">Manage User</span></a></li>
                    <li class="@if($module == 'leave_request') active @endif"><a class="d-flex align-items-center" href="/leave-request"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Leaves Approval">Leaves Approval</span></a></li>
                    <li class="@if($module == 'user_attendance') active @endif"><a class="d-flex align-items-center" href="/user-attendance"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Attendance">Attendance</span></a></li>
                </ul>
            </li>
            <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="credit-card"></i><span class="menu-title text-truncate" data-i18n="Expenses">Expenses</span></a>
                <ul class="menu-content">
                    <li class="@if($module == 'my_expenses') active @endif"><a class="d-flex align-items-center" href="/my-expenses"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="My Expenses">My Expenses</span></a></li>
                    <li class="@if($module == 'team_expenses') active @endif"><a class="d-flex align-items-center" href="/team-expenses"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Team Expenses">Team Expenses</span></a></li>
                    <li class="@if($module == 'expense_approval') active @endif"><a class="d-flex align-items-center" href="/expense-approval"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Approval Request">Approval Request</span></a></li>
                </ul>
            </li>

            <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="video"></i><span class="menu-title text-truncate" data-i18n="Meetings">Meetings</span></a>
                <ul class="menu-content">
                    <li class="@if($module == 'create_meeting') active @endif"><a class="d-flex align-items-center" href="/create-meeting"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Create Meeting">Create Meeting</span></a></li>
                    <li class="@if($module == 'manage_meetings') active @endif"><a class="d-flex align-items-center" href="/manage-meetings"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Manage Meetings">Manage Meetings</span></a></li>
                </ul>
            </li>
            <li class=" nav-item @if($module == 'chat') active @endif"><a class="d-flex align-items-center" href="/chat"><i data-feather="message-square"></i><span class="menu-title text-truncate" data-i18n="Chat">Chat</span></a></li>
            <li class=" nav-item @if($module == 'order_tracking') active @endif"><a class="d-flex align-items-center" href="/order-tracking"><i data-feather="navigation"></i><span class="menu-title text-truncate" data-i18n="Order Tracking">Order Tracking</span></a></li>

            <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather='dollar-sign'></i><span class="menu-title text-truncate" data-i18n="Payroll">Payroll</span></a>
                <ul class="menu-content">
                    <li class="@if($module == 'attendance') active @endif"><a class="d-flex align-items-center" href="/attendance"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Attendance">Attendance</span></a></li>
                    <li class="@if($module == 'leaves') active @endif"><a class="d-flex align-items-center" href="/leaves"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Leaves">Leaves</span></a></li>
                    <li class="@if($module == 'payslips') active @endif"><a class="d-flex align-items-center" href="/payslips"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Pay Slips">Pay Slips</span></a></li>
                </ul>
            </li>
            <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="life-buoy"></i><span class="menu-title text-truncate" data-i18n="Tickets">Tickets</span></a>
                <ul class="menu-content">
                    <li class="@if($module == 'create_ticket') active @endif"><a class="d-flex align-items-center" href="/create-ticket"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Attendance">Create Ticket</span></a></li>
                    <li class="@if($module == 'open_ticket') active @endif"><a class="d-flex align-items-center" href="/open-ticket"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Open Tickets">Open Tickets</span></a></li>
                    <li class="@if($module == 'tickets') active @endif"><a class="d-flex align-items-center" href="/tickets"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="All Tickets">All Tickets</span></a></li>
                </ul>
            </li>

            <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="users"></i><span class="menu-title text-truncate" data-i18n="Customer">Customer</span></a>
                <ul class="menu-content">
                    <li class="@if($module == 'add_customer') active @endif"><a class="d-flex align-items-center" href="/add-customer"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="New Customer">New Customer</span></a></li>
                    <li class="@if($module == 'manage_customer') active @endif"><a class="d-flex align-items-center" href="/manage-customer"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Manage Customer">Manage Customer</span></a></li>
                </ul>
            </li>


            <li class=" nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="file-text"></i><span class="menu-title text-truncate" data-i18n="Reports">Reports</span></a>
                <ul class="menu-content">
                    <li class="@if($module == 'sales_report') active @endif"><a class="d-flex align-items-center" href="/sales-report"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Sales Report">Sales Report</span></a></li>
                    <li class="@if($module == 'growth_report') active @endif"><a class="d-flex align-items-center" href="/growth-report"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Growth Report">Growth Report</span></a></li>
                </ul>
            </li>



        </ul>
    </div>
</div>