<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LabController extends Controller
{
    function index()
    {
        $page_data['module'] = 'login';
        return view('lab.login', $page_data);
    }

    function dashboard()
    {
        $page_data['module'] = 'dashboard';
        $page_data['module_title'] = 'Dashboard';
        return view('lab.dashboard', $page_data);
    }

    function add_product_type()
    {
        $page_data['module'] = 'add_product_type';
        $page_data['module_title'] = 'Add Product Type';
        return view('lab.add_product_type', $page_data);
    }

    function manage_product_type()
    {
        $page_data['module'] = 'manage_product_type';
        $page_data['module_title'] = 'Manage Product Type';
        return view('lab.manage_product_type', $page_data);
    }
    
    function manage_products()
    {
        $page_data['module'] = 'manage_products';
        $page_data['module_title'] = 'Manage Products';
        return view('lab.manage_products', $page_data);
    }

    function manage_orders()
    {
        $page_data['module'] = 'manage_orders';
        $page_data['module_title'] = 'Manage Orders';
        return view('lab.manage_orders', $page_data);
    }

    function add_sample()
    {
        $page_data['module'] = 'add_sample';
        $page_data['module_title'] = 'Add Sample';
        return view('lab.add_sample', $page_data);
    }

    function sample_request()
    {
        $page_data['module'] = 'sample_request';
        $page_data['module_title'] = 'Sample Request';
        return view('lab.sample_request', $page_data);
    }

    function sample_request_view()
    {
        $page_data['module'] = 'sample_request';
        $page_data['module_title'] = 'Sample Request';
        return view('lab.sample_request_view', $page_data);
    }

    function vendor_sample()
    {
        $page_data['module'] = 'vendor_sample';
        $page_data['module_title'] = 'Vendor Samples';
        return view('lab.vendor_sample', $page_data);
    }


    function view_matched_samples()
    {
        $page_data['module'] = 'view_matched_samples';
        $page_data['module_title'] = 'Matched Sample';
        return view('lab.view_matched_samples', $page_data);
    }

    function manage_samples()
    {
        $page_data['module'] = 'manage_samples';
        $page_data['module_title'] = 'Manage Samples';
        return view('lab.manage_samples', $page_data);
    }

    function pending_samples()
    {
        $page_data['module'] = 'pending_samples';
        $page_data['module_title'] = 'Add Sample';
        return view('lab.pending_samples', $page_data);
    }

    function add_documents()
    {
        $page_data['module'] = 'add_documents';
        $page_data['module_title'] = 'Add Document';
        return view('lab.add_documents', $page_data);
    }

    function manage_documents()
    {
        $page_data['module'] = 'manage_documents';
        $page_data['module_title'] = 'Manage Documents';
        return view('lab.manage_documents', $page_data);
    }






    function add_product()
    {
        $page_data['module'] = 'add_product';
        $page_data['module_title'] = 'Add Product';
        return view('lab.add_product', $page_data);
    }



    function chat()
    {
        $page_data['module'] = 'chat';
        return view('lab.chat', $page_data);
    }


    function tickets()
    {
        $page_data['module'] = 'tickets';
        $page_data['module_title'] = 'All Tickets';
        return view('lab.tickets', $page_data);
    }


    function open_tickets()
    {
        $page_data['module'] = 'open_ticket';
        $page_data['module_title'] = 'Open Tickets';
        return view('lab.open_ticket', $page_data);
    }

    function create_ticket()
    {
        $page_data['module'] = 'create_ticket';
        $page_data['module_title'] = 'Create Ticket';
        return view('lab.create_ticket', $page_data);
    }





    function attendance()
    {
        $page_data['module'] = 'attendance';
        $page_data['module_title'] = 'Attendance';
        return view('lab.attendance', $page_data);
    }


    function leaves()
    {
        $page_data['module'] = 'leaves';
        $page_data['module_title'] = 'Leaves';
        return view('lab.leaves', $page_data);
    }
    
    function payslips()
    {
        $page_data['module'] = 'payslips';
        $page_data['module_title'] = 'Payslips';
        return view('lab.payslips', $page_data);
    }
    
    function manage_meetings()
    {
        $page_data['module'] = 'manage_meetings';
        $page_data['module_title'] = 'Manage Meetings';
        return view('lab.manage_meetings', $page_data);
    }

    function create_meeting()
    {
        $page_data['module'] = 'create_meeting';
        $page_data['module_title'] = 'Create Meeting';
        return view('lab.create_meeting', $page_data);
    }




    
}
