<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SaleController extends Controller
{
    
    function index()
    {
        $page_data['module'] = 'login';
        return view('sales.login', $page_data);
    }

    function dashboard()
    {
        $page_data['module'] = 'dashboard';
        $page_data['module_title'] = 'Dashboard';
        return view('sales.dashboard', $page_data);
    }

    function new_product()
    {
        $page_data['module'] = 'new_product';
        $page_data['module_title'] = 'New Product';
        return view('sales.new_product', $page_data);
    }

    function view_product()
    {
        $page_data['module'] = 'view_product';
        $page_data['module_title'] = 'View Product';
        return view('sales.view_product', $page_data);
    }

    function chat()
    {
        $page_data['module'] = 'chat';
        return view('sales.chat', $page_data);
    }


    function tickets()
    {
        $page_data['module'] = 'tickets';
        $page_data['module_title'] = 'All Tickets';
        return view('sales.tickets', $page_data);
    }


    function open_tickets()
    {
        $page_data['module'] = 'open_ticket';
        $page_data['module_title'] = 'Open Tickets';
        return view('sales.open_ticket', $page_data);
    }

    function create_ticket()
    {
        $page_data['module'] = 'create_ticket';
        $page_data['module_title'] = 'Create Ticket';
        return view('sales.create_ticket', $page_data);
    }

    function stock_report()
    {
        $page_data['module'] = 'stock_report';
        $page_data['module_title'] = 'Stock Report';
        return view('sales.stock_report', $page_data);
    }

    function manage_orders()
    {
        $page_data['module'] = 'manage_orders';
        $page_data['module_title'] = 'Manage Orders';
        return view('sales.manage_orders', $page_data);
    }


    function create_order()
    {
        $page_data['module'] = 'create_order';
        $page_data['module_title'] = 'Create Order';
        return view('sales.create_order', $page_data);
    }

    function manage_offer()
    {
        $page_data['module'] = 'manage_offer';
        $page_data['module_title'] = 'Offer Management';
        return view('sales.manage_offer', $page_data);
    }

    function manage_users()
    {
        $page_data['module'] = 'manage_users';
        $page_data['module_title'] = 'Manage Users';
        return view('sales.manage_users', $page_data);
    }

    function add_user()
    {
        $page_data['module'] = 'add_user';
        $page_data['module_title'] = 'Add User';
        return view('sales.add_user', $page_data);
    }

    function leave_request()
    {
        $page_data['module'] = 'leave_request';
        $page_data['module_title'] = 'Leave Request';
        return view('sales.leave_request', $page_data);
    }


    function user_attendance()
    {
        $page_data['module'] = 'user_attendance';
        $page_data['module_title'] = 'User Attendance';
        return view('sales.user_attendance', $page_data);
    }



    function attendance()
    {
        $page_data['module'] = 'attendance';
        $page_data['module_title'] = 'Attendance';
        return view('sales.attendance', $page_data);
    }


    function leaves()
    {
        $page_data['module'] = 'leaves';
        $page_data['module_title'] = 'Leaves';
        return view('sales.leaves', $page_data);
    }
    
    function payslips()
    {
        $page_data['module'] = 'payslips';
        $page_data['module_title'] = 'Payslips';
        return view('sales.payslips', $page_data);
    }
    
    function manage_meetings()
    {
        $page_data['module'] = 'manage_meetings';
        $page_data['module_title'] = 'Manage Meetings';
        return view('sales.manage_meetings', $page_data);
    }

    function create_meeting()
    {
        $page_data['module'] = 'create_meeting';
        $page_data['module_title'] = 'Create Meeting';
        return view('sales.create_meeting', $page_data);
    }

    function expense_approval()
    {
        $page_data['module'] = 'expense_approval';
        $page_data['module_title'] = 'Expenses Approval';
        return view('sales.expense_approval', $page_data);
    }

    function team_expenses()
    {
        $page_data['module'] = 'team_expenses';
        $page_data['module_title'] = 'Team Expenses';
        return view('sales.team_expenses', $page_data);
    }


    function my_expenses()
    {
        $page_data['module'] = 'my_expenses';
        $page_data['module_title'] = 'My Expenses';
        return view('sales.my_expenses', $page_data);
    }

    function order_tracking()
    {
        $page_data['module'] = 'order_tracking';
        $page_data['module_title'] = 'Order Tracking';
        return view('sales.order_tracking', $page_data);
    }

    function manage_customer()
    {
        $page_data['module'] = 'manage_customer';
        $page_data['module_title'] = 'Manage Customer';
        return view('sales.manage_customer', $page_data);
    }

    function add_customer()
    {
        $page_data['module'] = 'add_customer';
        $page_data['module_title'] = 'Add Customer';
        return view('sales.add_customer', $page_data);
    }

    function sales_report()
    {
        $page_data['module'] = 'sales_report';
        $page_data['module_title'] = 'Sales Report';
        return view('sales.sales_report', $page_data);
    }

    function growth_report()
    {
        $page_data['module'] = 'growth_report';
        $page_data['module_title'] = 'Growth Report';
        return view('sales.growth_report', $page_data);
    }

    function sample_request()
    {
        $page_data['module'] = 'sample_request';
        $page_data['module_title'] = 'Sample Request';
        return view('sales.sample_request', $page_data);
    }

    function manage_sample()
    {
        $page_data['module'] = 'manage_sample';
        $page_data['module_title'] = 'Manage Samples';
        return view('sales.manage_sample', $page_data);
    }


}
