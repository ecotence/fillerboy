<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SaleController;
use App\Http\Controllers\LabController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::group(['domain' => 'sales.'.env('SITE_URL'), 'as' => 'sales'], function() {

Route::get('/', [SaleController::class, 'index']);

Route::get('/dashboard', [SaleController::class, 'dashboard']);

Route::get('/new-product', [SaleController::class, 'new_product']);
Route::get('/view-product', [SaleController::class, 'view_product']);

Route::get('/sample-request', [SaleController::class, 'sample_request']);
Route::get('/manage-sample', [SaleController::class, 'manage_sample']);

Route::get('/create-order', [SaleController::class, 'create_order']);
Route::get('/manage-orders', [SaleController::class, 'manage_orders']);
Route::get('/manage-offer', [SaleController::class, 'manage_offer']);

Route::get('/stock-report', [SaleController::class, 'stock_report']);

Route::get('/add-user', [SaleController::class, 'add_user']);
Route::get('/manage-users', [SaleController::class, 'manage_users']);
Route::get('/leave-request', [SaleController::class, 'leave_request']);
Route::get('/user-attendance', [SaleController::class, 'user_attendance']);

Route::get('/my-expenses', [SaleController::class, 'my_expenses']);
Route::get('/team-expenses', [SaleController::class, 'team_expenses']);
Route::get('/expense-approval', [SaleController::class, 'expense_approval']);

Route::get('/create-meeting', [SaleController::class, 'create_meeting']);
Route::get('/manage-meetings', [SaleController::class, 'manage_meetings']);

Route::get('/chat', [SaleController::class, 'chat']);
Route::get('/order-tracking', [SaleController::class, 'order_tracking']);


Route::get('/attendance', [SaleController::class, 'attendance']);
Route::get('/leaves', [SaleController::class, 'leaves']);
Route::get('/payslips', [SaleController::class, 'payslips']);

Route::get('/create-ticket', [SaleController::class, 'create_ticket']);
Route::get('/open-ticket', [SaleController::class, 'open_tickets']);
Route::get('/tickets', [SaleController::class, 'tickets']);

Route::get('/add-customer', [SaleController::class, 'add_customer']);
Route::get('/manage-customer', [SaleController::class, 'manage_customer']);

Route::get('/my-target', [SaleController::class, 'may_target']);
Route::get('/user-target', [SaleController::class, 'user_target']);

Route::get('/sales-report', [SaleController::class, 'sales_report']);
Route::get('/growth-report', [SaleController::class, 'growth_report']);



});


Route::group(['domain' => 'lab.'.env('SITE_URL'), 'as' => 'lab'], function() {
    Route::get('/', [LabController::class, 'index']);
    Route::get('/dashboard', [LabController::class, 'dashboard']);

    Route::get('/add-product-type', [LabController::class, 'add_product_type']);
    Route::get('/manage-product-type', [LabController::class, 'manage_product_type']);
    Route::get('/manage-products', [LabController::class, 'manage_products']);
    Route::get('/manage-orders', [LabController::class, 'manage_orders']);

    Route::get('/sample-request', [LabController::class, 'sample_request']);
    Route::get('/sample-request-view', [LabController::class, 'sample_request_view']);

    
    Route::get('/add-sample', [LabController::class, 'add_sample']);
    Route::get('/vendor-sample', [LabController::class, 'vendor_sample']);
    Route::get('/view-matched-samples', [LabController::class, 'view_matched_samples']);
    Route::get('/manage-samples', [LabController::class, 'manage_samples']);
    Route::get('/pending-samples', [LabController::class, 'pending_samples']);

    Route::get('/add-product', [LabController::class, 'add_product']);

    Route::get('/create-meeting', [LabController::class, 'create_meeting']);

    Route::get('/manage-meetings', [LabController::class, 'manage_meetings']);


    Route::get('/create-ticket', [LabController::class, 'create_ticket']);
    Route::get('/open-ticket', [LabController::class, 'open_tickets']);
    Route::get('/tickets', [LabController::class, 'tickets']);

    Route::get('/attendance', [LabController::class, 'attendance']);
    Route::get('/leaves', [LabController::class, 'leaves']);
    Route::get('/payslips', [LabController::class, 'payslips']);
    Route::get('/chat', [LabController::class, 'chat']);

    Route::get('/add-documents', [LabController::class, 'add_documents']);
    Route::get('/manage-documents', [LabController::class, 'manage_documents']);



    


});



